# NERSC Documentation

This repository contains NERSC [documentation](https://docs.nersc.gov/) written
in Markdown which is converted to html/css/js with the 
[mkdocs](http://www.mkdocs.org) static site generator. The theme is
[mkdocs-material](https://github.com/squidfunk/mkdocs-material) with 
NERSC customizations such as the colors injected via selective overloading of
templates and CSS. The documentation pages are found in the top-level folder
`docs`.

# Reporting Issues

To report a problem with our documentation please create an
[issue](https://gitlab.com/NERSC/nersc.gitlab.io/-/issues) 
and someone will work on your task. Before you create a ticket, please check if
there is an existing issue for the same problem to avoid duplicate issues.

If you need any further assistance, please consider reaching out to the NERSC
Consulting group by email at `consult@nersc.gov` or submit a ticket at the 
[NERSC help portal](https://help.nersc.gov).

# Contributing

We would welcome your contributions! In order to get started, check out the
[Contributing Guide](https://gitlab.com/NERSC/nersc.gitlab.io/-/blob/main/CONTRIBUTING.md)
and join the `#docs` Slack channel at nersc.slack.com.

# License

This repository is licensed under the BSD 3-Clause. For more details, see 
[LICENSE](https://gitlab.com/NERSC/nersc.gitlab.io/-/blob/main/LICENSE).
