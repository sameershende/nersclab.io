# Current known issues

## Perlmutter

!!! warning "Perlmutter is not a production resource"
    Perlmutter is not a production resource and usage is not charged
    against your allocation of time. While we will attempt to make the
    system available to users as much as possible, it is subject to
    unannounced and unexpected outages, reconfigurations, and periods
    of restricted access.

!!! danger "Recompile needed after Dec 6th update"
    All users should recompile their code following our 
    [compile instructions](systems/perlmutter/index.md#compilingbuilding-software). 
    Pease visit the [timeline page](systems/perlmutter/timeline/index.md#december-6-2021) for 
    details on things that were updated in this maintenance.    

NERSC has automated monitoring that tracks failed nodes, so please
only open tickets for node failures if the node consistently has poor
performance relative to other nodes in the job or if the node
repeatedly causes your jobs to fail.

- Batch system email notifications for jobs (including those for
  `scrontab`) are currently not enabled
- `PrgEnv-gnu` users if using a cuda enabled code (gcc and nvcc) one
  may load `cpe-cuda` module in order to get compatible version of `gcc` for the
  respective `cudatoolkit` installation. Please see our [gcc compatibility
  section](systems/perlmutter/index.md#gcc-compatibility-with-the-nvcc-compiler)
  for additional details.
- Static compiling isn't officially supported at NERSC, but some instructions
  can be found in the [compiler wrappers](development/compilers/wrappers.md#static-compilation)
  documentation page.
- CUDA-aware MPICH can only use up to half the resources on a CPU when
  it can see a GPU due to allocating GPU memory. Setting GPU binding will help
  distribute the CUDA-objects between GPUs to avoid an out of memory
  error.
- The lmod configuration for csh doesn't carry over to non-interactive
  logins (like batch scripts). This can be worked around by adding
  `source /usr/share/lmod/8.3.1/init/csh` to your `.tcshrc` file.
- Some users may see messages like `-bash:
  /usr/common/usg/bin/nersc_host: No such file or directory` when you
  login. This means you have outdated dotfiles that need to be
  updated. To stop this message, you can either delete this line from
  your dot files or check if `NERSC_HOST` is set before overwriting
  it. Please see our [environment
  page](environment/index.md#home-directories-shells-and-dotfiles)
  for more details.
- [Known issues for Machine Learning applications](machinelearning/known_issues.md)
- Users may notice MKL-based CPU code runs more slowly. Try
  `module load fast-mkl-amd`.
- Nodes on Perlmutter currently do not get a constant `hostid` (IP address) response.
- MPI/mpi4py users may notice a
  [mlx5 error](development/languages/python/using-python-perlmutter.md#known-issues)
  error that stems from spawning forks within an MPI rank, which is considered
  undefined/unsupported behavior.
- `collabsu` is not available. Please create a
  [direct login](accounts/collaboration_accounts.md#direct-login) with
  [sshproxy](systems/perlmutter/index.md#connecting-to-perlmutter-with-sshproxy)
  to login into Perlmutter
  or switch to a collaboration account on Cori and then login to Perlmutter.
- Spack on perlmutter has gotten out-of-date with reference to the installed compilers
  and other PE components, and consequently mostly does not work at the moment. We're
  working to update the configuration.
- Users may see the error: `[PE_0]:_pmi_applist_setup:App list is not supported for this job! Aborted`
  when running applications built with `pmi` libraries in them. Currently all the hdf5 utilities
  (`h5dump`, `h5stat`, etc) from the `cray-hdf5-parallel` module suffer from this bug.
  A temprary workaround is to request a compute node and preprend `srun -n 1` to the command to
  run.

!!! caution "Be careful with NVIDIA Unified Memory to avoid crashing nodes"
    In your code, [NVIDIA Unified Memory](https://developer.nvidia.com/blog/unified-memory-cuda-beginners/)
    might
    look something like `cudaMallocManaged`. At the moment, we do not have
    the ability to control this kind of memory and keep it under a safe
    limit. Users who allocate a large
    pool of this kind of memory may end up crashing nodes if the UVM
    memory does not leave enough room for necessary system tools like
    our filesystem client.
    We expect a fix in early 2022. In the meantime, please keep the size
    of memory pools allocated via UVM relatively small. If you have
    questions about this, please contact us.

## Cori 

The Burst Buffer on Cori has a number of known issues, documented at [Cori Burst Buffer](filesystems/cori-burst-buffer.md#known-issues).
