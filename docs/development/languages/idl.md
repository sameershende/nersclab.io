# IDL

IDL's primary use is in the analysis and display of scientific data through its
programming, plotting, and image display facilities.

## Features

* 2-D Plotting & Contouring
* Surface Plotting & 3-D Graphics
* Graphic Effects
* Color Systems
* Curve & Surface Fitting
* Image and Signal Processing
* Eigensystems
* Linear Systems
* Sparse Linear Systems
* Nonlinear Systems and Root Finding
* Multi-Dimensional Optimization
* Special & Transcendental Functions
* Correlation Analysis and Forecasting
* Hypothesis Testing
* Multi-Dimensional Gridding and Interpolation
* Mapping
* Development and Programming Tools
* Integrated Development Environment
* User Interface Toolkit
* IDL Insight
* IDL DataMiner Option

## Using IDL at NERSC

First load the IDL module

```
module load idl
```

and start IDL by running

```
idl
```

Or to use the IDL development environment, first log on to Cori via
[NoMachine](../../connect/nx.md) and then type:

```
module load idl
idlde
```

IDL has a rich set of demos. To view them, in the **IDL command line**, type:

```
demo
```

## Running IDL procedures via batch job

If you would prefer to run IDL via a batch job, here is a simple
demonstration workflow.

Let's assume we would like to run an IDL
procedure called `hello_idl.pro`:

```
pro idl_hello
print, 'Hello world from IDL'
end
```

Here is the batch script that will run our IDL procedure which we will call
`idl_batch.sh`. For more help setting up your batch script, check out our
jobscript [generator](https://my.nersc.gov/script_generator.php).

```slurm
#!/bin/bash
#SBATCH -N 1
#SBATCH -C knl
#SBATCH -q regular
#SBATCH -t 00:05:00

#OpenMP settings:
export OMP_NUM_THREADS=68
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

#load the idl module
module load idl

#run the application:
srun -n 1 -c 272 --cpu_bind=cores idl -e "idl_hello"
```

Then to submit your idl job, simply type

```
sbatch idl_batch.sh
```

`idl_batch.sh` will load IDL and execute the procedure that you specified. The
key is the `idl -e "idl_hello"` which allows IDL to execute your procedure via
the command line. We set `OMP_NUM_THREADS=68` and `-c 272` to give IDL the
ability to take advantage of multithreading which is availible in some
libraries. For more information about multithreading in IDL see
[here](https://www.harrisgeospatial.com/Support/Self-Help-Tools/Help-Articles/Help-Articles-Detail/ArtMID/10220/ArticleID/19291/3252).

## Availability at NERSC

We currently support IDL 8.5 (default), 8.3, and 8.2.

## Additional resources

For more information on IDL, please refer to the IDL software home
[page](https://www.harrisgeospatial.com/Software-Technology/IDL).
