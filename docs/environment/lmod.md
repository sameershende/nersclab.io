# Lmod

!!! note
    Lmod is available on the Perlmutter system
    
[Lmod](https://lmod.readthedocs.io/en/latest/index.html) is a Lua
based module system that helps manage the user environment (`PATH`,
`LD_LIBRARY_PATH`) through module files. Lmod is an extension of
[environment-modules](modules.md) that supports TCL modules along
with hierarchical `MODULEPATH`. Lmod is the moduling system on Perlmutter.

## About module

module is a bash function read by `$LMOD_CMD` which points to the lmod command
that reads module file and evaluates each module file using `eval` command:

```
elvis@perlmutter> type module
module is a function
module () 
{ 
    eval $($LMOD_CMD bash "$@") && eval $(${LMOD_SETTARG_CMD:-:} -s sh)
}
```

## Command Summary

| Command                            | Description                                               | 
| -----------------------------------| ----------------------------------------------------------|
| `module list`                      | List active modules in the user environment               |
| `module av [module]`               | List available modules in MODULEPATH                      |    
| `module load [module]`             | Load a module file in the users environment               |
| `module unload [module]`           | Remove a *loaded* module from the user environment        | 
| `module purge`                     | Remove all modules from the user environment              |
| `module swap [module1] [module2]`  | Replace `module1` with `module2`                          |         
| `module spider [module]`           | Query all modules in MODULEPATH and any module hierarchy  |
| `module show [module]`             | Show content of commands performed by loading module file |
| `module --raw show [module]`       | Show raw content of module file                           |  
| `module help [module]`             | Show help for a given module                              |
| `module whatis [module]`           | A brief description of the module, generally single line  |
| `module savelist`                  | List all user collections                                 |
| `module save [collection]`         | Save active modules in a user collection                  |
| `module describe [collection]`     | Show content of user collection                           |
| `module restore [collection]`      | Load modules from a collection                            |
| `module disable [collection]`      | Disable a user collection                                 |
| `module --config`                  | Show Lmod configuration                                   |
| `module use [-a] [path]`           | Prepend or Append path to MODULEPATH                      |
| `module unuse [path]`              | Remove path from MODULEPATH                               |
| `module --show_hidden av`          | Show all available modules in MODULEPATH including hidden modules       |
| `module --show_hidden spider`      | Show all possible modules in MODULEPATH and module hierarchy including hidden modules |

## ml 

Lmod provides another convenient shortcut command for the **module** command
called [ml](https://lmod.readthedocs.io/en/latest/010_user.html#ml-a-convenient-tool)
for user convenience, which mimics the `module` command. 

The `ml` command without arguments is equivalent to `module list` and any sub-commands to `module` command
are usable with the `ml` command. For instance `ml avail` is equivalent to `module avail`; `ml spider` 
is equivalent to `module spider`, etc.

The syntax for loading and unloading modules with the `ml` command differs from the `module` command.
You can type `ml <module>` to load a module, and unload by prepending a minus sign (-): `ml -<module>`.

For instance if we want to load the `gcc` module we can run `ml gcc`. To remove this module from the
set of loaded modules we would run `ml -gcc`. 

```console
elvis@perlmutter> ml gcc
elvis@perlmutter> ml

Currently Loaded Modules:
  1) gcc/10.2.0 (c)

  Where:
   c:  Compiler
```  

One can load and unload module in a single `ml` command: for instance if you want to unload `gcc` and load
`cuda` you could run: `ml -gcc cuda` or `ml cuda -gcc`. 

## User Collections

Lmod introduced the concept of [user collections](https://lmod.readthedocs.io/en/latest/010_user.html#user-collections),
allowing a user to reference a group of modules with a unique name. This is particularly useful if there
is a set of modules you load regularly to do a particular task. You can save the modules into a collection to
save the trouble of retyping them every time, loading them with the `module restore` command. Note that Lmod 
can load only one user collection at a time.

In order to save modules in a collection, we will load some modules and run `module save`. In this example,
we save our active modules into the `default` collection.  

```console
elvis@perlmutter> module purge
elvis@perlmutter> module load gcc
elvis@perlmutter> module list

Currently Loaded Modules:
  1) gcc/10.2.0 (c)

  Where:
   c:  Compiler

 

elvis@perlmutter> module save
Saved current collection of modules to: "default", for system: "perlmutter"
```

Lmod will store user collection in **$HOME/.lmod.d**. You can view all collections using `module savelist`.

```console
elvis@perlmutter> module savelist
Named collection list (For LMOD_SYSTEM_NAME = "perlmutter"):
  1) default
```

You can see contents of a collection using `module describe`, which shows the modules that will be loaded
when restoring from the collection. In this next example, we will purge and restore from the collection, 
which will simply load the `gcc` module:

```console
elvis@perlmutter> module describe
Collection "default" contains: 
   1) gcc
elvis@perlmutter> module purge
elvis@perlmutter> module list
No modules loaded
elvis@perlmutter> module restore 
Restoring modules from user's default, for system: "perlmutter"
elvis@perlmutter> module list

Currently Loaded Modules:
  1) gcc/10.2.0 (c)

  Where:
   c:  Compiler
```

## Showing the contents of module files

There are several commands to reveal the contents of module files: `module whatis`, 
`module help`, `module show`, and `module spider`.

The `module whatis` is a single line summary of module file and `module help` is a multi-line description of the module file.

The `module show` command displays the commands executed when loading the module (`module load`).  Shown below are commands run
in your user shell when loading `PrgEnv-nvidia` module. 

!!! note

    The output of `module show` is not the content of the module file. If you want to see the content of the module file, add 
    the `--raw` option: `module --raw show PrgEnv-nvidia`.

```console

elvis@perlmutter> module show PrgEnv-nvidia
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   /opt/cray/pe/lmod/modulefiles/core/PrgEnv-nvidia/8.0.0.lua:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
family("PrgEnv")
help([[
Use 'module restore PrgEnv-nvidia' to load the default nvidia programming environment.

]])
whatis("Enables the Programming Environment using the nvidia compilers.")
setenv("PE_ENV","NVIDIA")
load("nvidia")
load("craype")
load("craype-x86-rome")
load("craype-network-ofi")
load("cray-dsmml")
load("perftools-base")
load("xpmem")
load("cray-mpich")
load("cray-libsci")
```

The `module spider` command reports all modules in your system in MODULEPATH along with all module trees in the hierarchical 
system. Note that `module avail` doesn't show modules from all trees in the hierarchical system. If you want to know **all** 
available software on the system, please use `module spider`. 

The output will be a list of software entries with corresponding versions:

```console
elvis@perlmutter> module spider

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
The following is a list of the modules and extensions currently available:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  aocc: aocc/2.2.0.1

  atp: atp/3.13.1

  cce: cce/11.0.4

  clingo: clingo/git_20210514

  cmake: cmake/3.18.4

  cpe: cpe/21.04

  cray-ccdb: cray-ccdb/4.11.1

  cray-cti: cray-cti/2.13.6

  cray-dsmml: cray-dsmml/0.1.4

  cray-fftw: cray-fftw/3.3.8.9

  cray-hdf5: cray-hdf5/1.12.0.3

  ...
``` 

The `module spider` can report all versions of the software. For instance if we want to see all
gcc compilers we can run the following. In this example we have three versions of gcc (**8.1.0**,
**9.3.0**, and **10.2.0**):

```console
elvis@perlmutter> module spider gcc

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  gcc:
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     Versions:
        gcc/8.1.0
        gcc/9.3.0
        gcc/10.2.0

------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  For detailed information about a specific "gcc" package (including how to load the modules) use the module's full name.
  Note that names that have a trailing (E) are extensions provided by other modules.
  For example:

     $ module spider gcc/10.2.0
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
```

!!! note
    We recommend you review output of `module --show_hidden spider`, which reports all available modules in MODULEPATH,
    including hidden modules. 
    
## Lmod Configuration for Perlmutter

We have made a few changes to Lmod configuration which are described below. 

!!! note
    Lmod configuration files can be found at `$LMOD_CONFIG_DIR`. If you want to view Lmod configuration
    you can run `module --config`. Please see https://lmod.readthedocs.io/en/latest/090_configuring_lmod.html for 
    an overview of Lmod configuration values and their description.
    
### Format Titles for Module Trees

We have set the environment `LMOD_AVAIL_STYLE="grouped:system"` which alters the output of `module avail`. with
this change, Lmod displays titles chosen by NERSC for each module tree, rather than the directory path as is its 
default behavior.

If you run `module avail` you will see a title next for each module tree 
    
??? "module avail with grouped style" 
    
    ```
    elvis@perlmutter> module av
    
    ----------------------------------------------------------------- Cray Compiler/Network Dependent Packages -----------------------------------------------------------------
       cray-mpich-abi/8.1.4    cray-mpich/8.1.4
    
    --------------------------------------------------------------------- Cray Compiler Dependent Modules ----------------------------------------------------------------------
       cray-hdf5/1.12.0.3
    
    ---------------------------------------------------------------------- Additional available software -----------------------------------------------------------------------
       clingo/git_20210514    darshan/3.2.1       mpich/3.4.1        npe-llvm/0.1              py-cffi/1.14.3                 spack/0.16.1
       cmake/3.18.4           fast-mkl-amd/1.0    nccl/2.9.6         nvidia-nersc/20.11        python/3.8-anaconda-2020.11    xalt/2.10.2
       cudnn/8.2.0            llvm/11.0.1         nccl/2.9.8  (D)    nvidia-nersc/21.5  (D)    pytorch/1.8.0
    
    ----------------------------------------------------------------------------- Lmod Modulefiles -----------------------------------------------------------------------------
       lmod    settarg
    
    ------------------------------------------------------------------------ Cray MPI Dependent Modules ------------------------------------------------------------------------
       cray-hdf5-parallel/1.12.0.3    cray-hdf5-parallel/1.12.0.3 (D)    cray-parallel-netcdf/1.12.1.3 (D)    cray-parallel-netcdf/1.12.1.3
    
    ---------------------------------------------------------------------------- Cray Core Modules -----------------------------------------------------------------------------
       aocc/2.2.0.1     (D)    cray-dsmml/0.1.4         cray-R/4.0.3.0        (D)    gcc/10.2.0              (L,D)    nvidia/20.9            (D)    PrgEnv-nvidia/8.0.0
       atp/3.13.1              cray-jemalloc/5.1.0.4    cray-stat/4.10.1             gdb4hpc/4.12.5                   papi/6.0.0.6                  valgrind4hpc/2.11.1
       cce/11.0.4              cray-libsci/21.04.1.1    craype/2.7.6                 iobuf/2.0.10                     perftools-base/21.02.0
       cpe/21.04               cray-pmi-lib/6.0.10      craypkg-gen/1.3.14           nvhpc-byo-compiler/20.9 (D)      PrgEnv-aocc/8.0.0
       cray-ccdb/4.11.1        cray-pmi/6.0.10          cudatoolkit/20.9_11.0 (D)    nvhpc-nompi/20.9        (D)      PrgEnv-cray/8.0.0
       cray-cti/2.13.6         cray-python/3.8.5.0      gcc/9.3.0                    nvhpc/20.9              (D)      PrgEnv-gnu/8.0.0
    
    ----------------------------------------------------------- /opt/cray/pe/lmod/modulefiles/craype-targets/default -----------------------------------------------------------
       craype-accel-amd-gfx908    craype-accel-nvidia80    craype-hugepages1G      craype-hugepages2M     craype-hugepages512M    craype-network-none    craype-x86-milan
       craype-accel-host          craype-hugepages128M     craype-hugepages256M    craype-hugepages32M    craype-hugepages64M     craype-network-ofi     craype-x86-rome
       craype-accel-nvidia70      craype-hugepages16M      craype-hugepages2G      craype-hugepages4M     craype-hugepages8M      craype-network-ucx
    
    ------------------------------------------------------------------------------- Cray Modules -------------------------------------------------------------------------------
       cray-ucx/2.7.0-1    cudatoolkit/20.9_11.0    libfabric/1.11.0.3.66    nvhpc-byo-compiler/20.9    nvhpc-nompi/20.9    nvhpc/20.9
    
    ------------------------------------------------------------------------------ Cray Compilers ------------------------------------------------------------------------------
       aocc/2.2.0.1    cray-R/4.0.3.0    gcc/8.1.0    gcc/9.3.0    gcc/10.2.0    nvidia/20.9
    
    ------------------------------------------------------------------------- NERSC-provided Software --------------------------------------------------------------------------
       Default
    
      Where:
       L:  Module is loaded
       D:  Default Module
    
    Use "module spider" to find all possible modules and extensions.
    Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".

    ```

If you want to see the full path to module tree you can use `-s <system>` option which sets Lmod avail style to prefer `system` 
instead of `grouped`. Alternately you can set the environment `LMOD_AVAIL_STYLE="system:grouped"` which will make the change
persistent in your shell. Shown below is an output using the `system` avail format.
    
??? "module avail with system style"
    
    ```console
    elvis@perlmutter> module -s system av
    
    ----------------------------------------------------------- /opt/cray/pe/lmod/modulefiles/comnet/gnu/8.0/ofi/1.0 -----------------------------------------------------------
       cray-mpich-abi/8.1.4    cray-mpich/8.1.4
    
    -------------------------------------------------------------- /opt/cray/pe/lmod/modulefiles/compiler/gnu/8.0 --------------------------------------------------------------
       cray-hdf5/1.12.0.3
    
    -------------------------------------------------------- /global/common/software/nersc/shasta2105/extra_modulefiles --------------------------------------------------------
       clingo/git_20210514    darshan/3.2.1       mpich/3.4.1        npe-llvm/0.1              py-cffi/1.14.3                 spack/0.16.1
       cmake/3.18.4           fast-mkl-amd/1.0    nccl/2.9.6         nvidia-nersc/20.11        python/3.8-anaconda-2020.11    xalt/2.10.2
       cudnn/8.2.0            llvm/11.0.1         nccl/2.9.8  (D)    nvidia-nersc/21.5  (D)    pytorch/1.8.0
    
    ------------------------------------------------------------------ /usr/share/lmod/lmod/modulefiles/Core -------------------------------------------------------------------
       lmod    settarg
    
    ---------------------------------------------------- /opt/cray/pe/lmod/modulefiles/mpi/nvidia/20/ofi/1.0/cray-mpich/8.0 ----------------------------------------------------
       cray-hdf5-parallel/1.12.0.3    cray-parallel-netcdf/1.12.1.3 (D)
    
    ------------------------------------------------- /opt/cray/pe/lmod/modulefiles/mpi/crayclang/10.0/ofi/1.0/cray-mpich/8.0 --------------------------------------------------
       cray-hdf5-parallel/1.12.0.3 (D)    cray-parallel-netcdf/1.12.1.3
    
    -------------------------------------------------------------------- /opt/cray/pe/lmod/modulefiles/core --------------------------------------------------------------------
       aocc/2.2.0.1     (D)    cray-dsmml/0.1.4         cray-R/4.0.3.0        (D)    gcc/10.2.0              (L,D)    nvidia/20.9            (D)    PrgEnv-nvidia/8.0.0
       atp/3.13.1              cray-jemalloc/5.1.0.4    cray-stat/4.10.1             gdb4hpc/4.12.5                   papi/6.0.0.6                  valgrind4hpc/2.11.1
       cce/11.0.4              cray-libsci/21.04.1.1    craype/2.7.6                 iobuf/2.0.10                     perftools-base/21.02.0
       cpe/21.04               cray-pmi-lib/6.0.10      craypkg-gen/1.3.14           nvhpc-byo-compiler/20.9 (D)      PrgEnv-aocc/8.0.0
       cray-ccdb/4.11.1        cray-pmi/6.0.10          cudatoolkit/20.9_11.0 (D)    nvhpc-nompi/20.9        (D)      PrgEnv-cray/8.0.0
       cray-cti/2.13.6         cray-python/3.8.5.0      gcc/9.3.0                    nvhpc/20.9              (D)      PrgEnv-gnu/8.0.0
    
    ----------------------------------------------------------- /opt/cray/pe/lmod/modulefiles/craype-targets/default -----------------------------------------------------------
       craype-accel-amd-gfx908    craype-accel-nvidia80    craype-hugepages1G      craype-hugepages2M     craype-hugepages512M    craype-network-none    craype-x86-milan
       craype-accel-host          craype-hugepages128M     craype-hugepages256M    craype-hugepages32M    craype-hugepages64M     craype-network-ofi     craype-x86-rome
       craype-accel-nvidia70      craype-hugepages16M      craype-hugepages2G      craype-hugepages4M     craype-hugepages8M      craype-network-ucx
    
    -------------------------------------------------------------------------- /opt/cray/modulefiles ---------------------------------------------------------------------------
       cray-ucx/2.7.0-1    cudatoolkit/20.9_11.0    libfabric/1.11.0.3.66    nvhpc-byo-compiler/20.9    nvhpc-nompi/20.9    nvhpc/20.9
    
    ----------------------------------------------------------------------------- /opt/modulefiles -----------------------------------------------------------------------------
       aocc/2.2.0.1    cray-R/4.0.3.0    gcc/8.1.0    gcc/9.3.0    gcc/10.2.0    nvidia/20.9
    
    ----------------------------------------------------------- /global/common/software/nersc/shasta2105/modulefiles -----------------------------------------------------------
       Default
    
      Where:
       L:  Module is loaded
       D:  Default Module
    
    Use "module spider" to find all possible modules and extensions.
    Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".
    ```

### Lmod Families

Lmod introduces the concept of [families](https://lmod.readthedocs.io/en/latest/050_lua_modulefiles.html) with the 
`family("name")` Lua function in which modules can belong to a family group and only one module from the family group can be 
loaded at a given time. This can be useful to help protect users from loading two MPI libraries such as `openmpi` and `mpich` 
that provide the same binaries `mpicc`, `mpifort`, `mpic++`. 

HPE/Cray has defined several families in their software stack. The `compiler` family contains the `gcc`, `cce`,
`aocc` and `nvidia` modules. Lmod will automatically swap modules when you load modules from same family group. Shown below we 
see Lmod swaps `nvidia` with `aocc` and `aocc` with `gcc` since Lmod permits only one module from the `compiler` family 
to be loaded at a time.

```console
elvis@perlmutter> module load aocc

Lmod is automatically replacing "nvidia/20.9" with "aocc/2.2.0.1".

elvis@perlmutter> module load gcc

Lmod is automatically replacing "aocc/2.2.0.1" with "gcc/10.2.0".

```

A quick way to check the active families is to run `module --mt`, which shows the module tables state along with family names. 
In the below output we see there are five families defined: `PrgEnv`, `compiler`, `craype`, `craype_cpu`, and `craype_network`.

```console
elvis@perlmutter> module --mt
_ModuleTable_ = {
  ["MTversion"] = 3,
  ["c_rebuildTime"] = 7200.0,
  ["c_shortTime"] = 0.53126287460327,
  depthT = {},
  family = {
    ["PrgEnv"] = "PrgEnv-nvidia",
    ["compiler"] = "gcc",
    ["craype"] = "craype",
    ["craype_cpu"] = "craype-x86-rome",
    ["craype_network"] = "craype-network-ofi",
  },
...
```

Lmod will set the environment variables `LMOD_FAMILIY_<NAME>` and `LMOD_FAMILY_<NAME>_VERSION`, where `<name>` is
the name of family that can be used to reference family. 

### Module Properties

We have configured [module properties](https://lmod.readthedocs.io/en/latest/145_properties.html) for a subset of modules to
help classify the software provided. If you run `module list`, you will see properties such as `cpe`, `math`, `io`,
`c`, `dev` to help you identify what type of software the module provides.
 
```console
elvis@perlmutter> module list

Currently Loaded Modules:
  1) nvidia/20.9     (g,c)   4) libfabric/1.11.0.3.66   7) perftools-base/21.02.0                    (dev)   10) PrgEnv-nvidia/8.0.0 (cpe)  13) cray-pmi/6.0.10
  2) craype/2.7.6    (c)     5) craype-network-ofi      8) xpmem/2.2.40-7.0.1.0_1.9__g1d7a24d.shasta (H)     11) xalt/2.10.2                14) cray-pmi-lib/6.0.10
  3) craype-x86-rome         6) cray-dsmml/0.1.4        9) cray-libsci/21.04.1.1                     (math)  12) darshan/3.2.1       (io)   15) Default

  Where:
   g:     built for GPU
   cpe:   Cray Programming Environment Modules
   math:  Mathematical libraries
   io:    Input/output software
   c:     Compiler
   dev:   Development Tools and Programming Languages
   H:                Hidden Module
```

### Startup module 

We have configured Lmod to provide a module that is loaded by default for all users, 
called **Default**. This module can be loaded using `module load Default`. If you are unsure how to 
get to the startup configuration you can do one of the following:

1. Run `module restore`
2. Run `module purge && module load Default`

!!! note

    If you have a default collection (a collection entitled `default`), then  `module restore` will restore your 
    user collection instead of the system default. You can check your collections by running `module savelist` or 
    examining the content of the files in **$HOME/.lmod.d**

If you don't have a default user collection, then Lmod will restore from the startup module file `Default`. This
is done by Lmod by reading environment `LMOD_SYSTEM_DEFAULT_MODULES` which should not be changed. 

```console
elvis@perlmutter> module purge
elvis@perlmutter> module restore
Resetting modules to system default. Reseting $MODULEPATH back to system default. All extra directories will be removed from $MODULEPATH.
elvis@perlmutter> ml

Currently Loaded Modules:
  1) cray-mpich/8.1.4      (mpi)   7) cray-dsmml/0.1.4                                  13) darshan/3.2.1       (io)
  2) nvidia/20.9           (g,c)   8) perftools-base/21.02.0                    (dev)   14) cray-pmi/6.0.10
  3) craype/2.7.6          (c)     9) xpmem/2.2.40-7.0.1.0_1.9__g1d7a24d.shasta (H)     15) cray-pmi-lib/6.0.10
  4) craype-x86-rome              10) cray-libsci/21.04.1.1                     (math)  16) Default
  5) libfabric/1.11.0.3.66        11) PrgEnv-nvidia/8.0.0                       (cpe)
  6) craype-network-ofi           12) xalt/2.10.2

  Where:
   g:     built for GPU
   mpi:   MPI Providers
   cpe:   Cray Programming Environment Modules
   math:  Mathematical libraries
   io:    Input/output software
   c:     Compiler
   dev:   Development Tools and Programming Languages
   H:                Hidden Module
```

Please refer to see https://lmod.readthedocs.io/en/latest/070_standard_modules.html for details 
related to startup modules 

## Useful Tips 

### Redirecting Module Output

Lmod will redirect output to stderr by default, which means this won't work as you expected, since its output 
will not be stored in a file.

```console
elvis@perlmutter> module list > active.txt

Currently Loaded Modules:
  1) craype/2.7.6          (c)   5) cray-dsmml/0.1.4                                   9) PrgEnv-nvidia/8.0.0 (cpe)  13) cray-pmi-lib/6.0.10
  2) craype-x86-rome             6) perftools-base/21.02.0                    (dev)   10) xalt/2.10.2                14) Default
  3) libfabric/1.11.0.3.66       7) xpmem/2.2.40-7.0.1.0_1.9__g1d7a24d.shasta (H)     11) darshan/3.2.1       (io)   15) gcc/10.2.0          (c)
  4) craype-network-ofi          8) cray-libsci/21.04.1.1                     (math)  12) cray-pmi/6.0.10

  Where:
   cpe:   Cray Programming Environment Modules
   math:  Mathematical libraries
   io:    Input/output software
   c:     Compiler
   dev:   Development Tools and Programming Languages
   H:                Hidden Module
```

The `--redirect` option to any module command will redirect stderr to stdout so that shell commands 
can capture module commands. 

```console
elvis@perlmutter> module --redirect list > active.txt
elvis@perlmutter> cat active.txt 

Currently Loaded Modules:
  1) craype/2.7.6          (c)   5) cray-dsmml/0.1.4                                   9) PrgEnv-nvidia/8.0.0 (cpe)  13) cray-pmi-lib/6.0.10
  2) craype-x86-rome             6) perftools-base/21.02.0                    (dev)   10) xalt/2.10.2                14) Default
  3) libfabric/1.11.0.3.66       7) xpmem/2.2.40-7.0.1.0_1.9__g1d7a24d.shasta (H)     11) darshan/3.2.1       (io)   15) gcc/10.2.0          (c)
  4) craype-network-ofi          8) cray-libsci/21.04.1.1                     (math)  12) cray-pmi/6.0.10

  Where:
   cpe:   Cray Programming Environment Modules
   math:  Mathematical libraries
   io:    Input/output software
   c:     Compiler
   dev:   Development Tools and Programming Languages
   H:                Hidden Module
```

### Autoswap modules of the same name

Lmod will automatically swap modules of the same name. For instance, if you load `gcc/9.3.0`, Lmod will remove `gcc/10.2.0` from 
your user environment:

```console
elvis@perlmutter> module load gcc/9.3.0 

The following have been reloaded with a version change:
  1) gcc/10.2.0 => gcc/9.3.0
```

### Debugging Modules

If you want to debug the state of modules, any of these command options can help:

- Tracing modules: `module -T`
- Print Module Table: `module --mt`
- Debug Level: `module --debug=[1|2|3]` 

To debug module files themselves, please see https://lmod.readthedocs.io/en/latest/160_debugging_modulefiles.html 
 
### Parsing output 

Use the `-t` option with  `module avail`, `module spider`, `module list`, `module spider` and `module savelist` commands 
to produce parsable output. 

To see loaded modules in a parsable format, the environment variable **LOADEDMODULES** is a colon separated
list of active modules loaded in your shell.

```console
elvis@perlmutter> echo $LOADEDMODULES
gcc/10.2.0:dvs/2.12_4.0.102-7.0.1.0_8.1__g30d29e7a
```

Similarly, the full path to the module files of active modules can be retrieved using the `_LMFILES_` environment
variable. The output is a colon separated list of module files.

```console
elvis@perlmutter> echo $_LMFILES_
/opt/cray/pe/lmod/modulefiles/core/gcc/10.2.0.lua:/opt/cray/modulefiles/dvs/2.12_4.0.102-7.0.1.0_8.1__g30d29e7a
```

### Seeing Defaults for module files

The `module -d avail` command will report the default for every module. Lmod will load the 
default module if you don't specify the full version (i.e., `module load gcc`) since there is 
only one default for every module name. 

For example, to list the default for `gcc`:

```console
elvis@perlmutter> module -d avail gcc

---------------------------------------------------------------------------- Cray Core Modules -----------------------------------------------------------------------------
   gcc/10.2.0 (L)

  Where:
   L:  Module is loaded

Use "module spider" to find all possible modules and extensions.
Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".
```
 
### Hidden modules

Hidden modules are module files with a leading dot (`.`) in front of the version. These modules are not shown when running
`module avail` or `module spider`. In order to see these modules you need to use the `--show_hidden` option. This option
is also required if you want to show the content of a hidden module file: `module --show_hidden show <name>`, 
`module --show_hidden help <name>`, or `module --show_hidden whatis <name>`.

A hidden module will have a symbol **(H)** next to the module file. 

In example below we have **dvs** as a hidden module which is not reported in the output of `module avail` 

```console
elvis@perlmutter> module avail dvs
No module(s) or extension(s) found!
Use "module spider" to find all possible modules and extensions.
Use "module keyword key1 key2 ..." to search for all possible modules matching any of the "keys".



elvis@perlmutter> module --show_hidden avail dvs

------------------------------------------------------------------------------- Cray Modules -------------------------------------------------------------------------------
   dvs/2.12_4.0.102-7.0.1.0_8.1__g30d29e7a (H)

  Where:
   H:  Hidden Module

```

For Perlmutter, we have configured our own system defaults and hidden modules in the
**modulerc.lua** file, which can be retrieved by the environment variable **$LMOD_MODULERCFILE**. 

### Managing User Collection

Lmod will store user collections in `$HOME/.lmod.d`, so if you want to **delete** a user collection, 
you can simplify delete the file in the `$HOME/.lmod.d` directory.

Shown below we have a `default` collection stored this in the file `$HOME/.lmod.d/default.perlmutter`. 

```console
elvis@perlmutter> ls ~/.lmod.d/
default.perlmutter
elvis@perlmutter> module -t savelist
default
``` 

If you want to delete the user collection `default`, you can remove the file

```console
elvis@perlmutter> rm ~/.lmod.d/default.perlmutter
elvis@perlmutter>  module describe default
Lmod Warning:  No collection named "default" found. 
``` 

Alternatively you can use `module disable [collection]` to disable a collection name. Lmod will 
rename the collection name by appending a `~` to the collection name so that 
Lmod won't read the collection. In the below example we create a user collection named `gcc` and 
running `module disable gcc` will rename the collection to `gcc.perlmutter~`; however Lmod doesn't 
recognize the *gcc* collection when running `module describe gcc`. To re-enable the user collection,
rename the file by removing the `~`.

```console
elvis@perlmutter> module purge
elvis@perlmutter> module load gcc
elvis@perlmutter> module save gcc
Saved current collection of modules to: "gcc", for system: "perlmutter"

elvis@perlmutter> ls ~/.lmod.d/
gcc.perlmutter
elvis@perlmutter> module disable gcc
Disabling gcc collection by renaming with a "~"
elvis@perlmutter> login38:ls ~/.lmod.d/
gcc.perlmutter~
elvis@perlmutter> module describe gcc
Lmod Warning:  No collection named "gcc" found. 
```

## Troubleshooting user environment issues

If you have issues with your user environment, please review your startup configuration files 
for any `module` commands. Look at the files sourced as a result of your configuration.
 
- bash/sh users: `$HOME/.bashrc`, `$HOME/.bashrc_profile`, `$HOME/.profile`
- csh/tcsh shell: `$HOME/.cshrc` 

In particular, look for changes to environment variable **MODULEPATH** or use of 
`module use` command in your startup configuration. 

## References

Please see the references below for additional help. NERSC's 
[Lmod Training](https://gitlab.com/NERSC/lmod-training) (the final link below) 
is a self-paced exercise covering the basics of Lmod and Lua module files in a 
Docker container.  

- Documentation: https://lmod.readthedocs.io/en/latest/index.html
- User Guide: https://lmod.readthedocs.io/en/latest/010_user.html
- GitHub: https://github.com/TACC/Lmod
- FAQ: https://lmod.readthedocs.io/en/latest/040_FAQ.html
- Lmod Training: https://gitlab.com/NERSC/lmod-training
