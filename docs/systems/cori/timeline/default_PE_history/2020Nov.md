# Programming Environment Change on Cori in Nov 2020

## Background

Following the Cori scheduled maintenance on November 17, 2020, we will
install the new Cray Programming Environment Software release
[CDT/20.10](
https://pubs.cray.com/bundle/RELEASED_HPE_Cray_Programming_Environment_20.10_for_Cray_XC_x86_Systems_October_8_2020/resource/RELEASED_HPE_Cray_Programming_Environment_20.10_for_Cray_XC_x86_Systems_October_8_2020.pdf),
and retire the old [CDT/20.03](
https://pubs.cray.com/bundle/Released_Cray_XC_-x86-_Programming_Environments_20.03_March_5_2020/resource/Released_Cray_XC_(x86)_Programming_Environments_20.03_March_5_2020.pdf
) release.  In addition, we will install the Intel compiler version
19.1.2.254 (which is the release version 2020 update 2). Software
default versions will remain the same.

Below is the detailed list of changes after the maintenance.

## New software versions available

* intel/19.1.2.254
* atp/3.8.1
* cce/10.0.3
* cray-R/4.0.2.0
* cray-ccdb/4.8.1
* cray-cti/2.8.1
* cray-fftw/3.3.8.8
* cray-hdf5, cray-hdf5-parallel/1.12.0.0
* cray-jemalloc/5.1.0.3
* cray-libsci, cray-libsci_acc/20.09.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.16
* cray-netcdf, cray-netcdf-hdf5parallel/4.7.4.0
* cray-parallel-netcdf/1.12.1.0
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.13.3.0
* cray-python/3.8.5.0
* cray-stat/4.7.2
* cray-trilinos/12.18.1.1
* craype/2.7.2
* craype-dl-plugin-py3/20.10.1
* craypkg-gen/1.3.11
* gcc/10.1.0
* gdb4hpc/4.8.1
* papi/6.0.0.4
* perftools-base/20.10.0
* pmi, pmi-lib/5.0.17
* valgrind4hpc/2.8.1

## Old software versions to be removed

* cce/9.1.3
* cray-R/3.6.1
* cray-cti/1.0.9
* cray-fftw/3.3.8.5
* cray-hdf5, cray-hdf5-parallel/1.10.6.0
* cray-libsci/20.03.1
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.12
* cray-netcdf/4.7.3.2
* cray-netcdf-hdf5parallel/4.7.3.2
* cray-parallel-netcdf/1.12.0.0
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.12.4.0
* cray-python/3.7.3.2
* cray-tpsl, cray-tpsl-64/20.03.1
* craype/2.6.4
* craype-dl-plugin-py3/19.12.1
* craypkg-gen/1.3.7
* gcc/9.2.0
* iobuf/2.0.9
* papi/5.7.0.3
* perftools-base/20.03.0
* pmi, pmi-lib/5.0.15
* stat/3.0.1.3
* valgrind4hpc/1.0.0
